<?php
include_once "../Model/Contato.class.php";
include_once "../Model/ContatoDAO.class.php";
session_start();
$usuario = $_SESSION["usuario"];

$contatoDAO = new ContatoDAO();

if(isset($_POST['acao'])){
    if($_POST['acao'] == "Cadastrar"){
    
     $idusuario     =    trim(strip_tags($_POST['idusuario']));
     $nome          =    trim(strip_tags($_POST['nome']));
     $tipo          =   trim(strip_tags($_POST['tipo']));
     $telefone      =   trim(strip_tags($_POST['telefone']));
     
     

    $contato = new Contato();
    $contato->setUsuario_idusuario($idusuario );
    $contato->setNome($nome);
    $contato->setTipo($tipo);
    $contato->setTelefone($telefone);

               
        if($contatoDAO->inserir($contato)){
            echo "<script>alert('cadastrado com sucesso!');</script>";
            session_start();
            $listar=$contatoDAO->listar_todos($usuario["idusuario"]);
            $_SESSION['contatos'] = $listar;
            header("Location: http://localhost/agenda_eletronica/View/admin/admin.php");
        }
        else{
            echo "<script>alert('cadastrado com sucesso!');</script>";
            session_start();
            
            $listar=$contatoDAO->listar_todos($usuario["idusuario"]);
            $_SESSION['contatos'] = $listar; 
            header("Location: http://localhost/agenda_eletronica/View/admin/admin.php");
        }

    }
    if($_POST['acao'] == "salvar_edicao"){
       
    
    $idcontato  =    trim(strip_tags($_POST['idcontato']));
    $nome       =    trim(strip_tags($_POST['nome']));
    $tipo       =   trim(strip_tags($_POST['tipo']));
    $telefone   =   trim(strip_tags($_POST['telefone']));
        
    $contato = new Contato();
    $contato->setIdcontato($idcontato);
    $contato->setNome($nome);
    $contato->setTipo($tipo);
    $contato->setTelefone($telefone);

    
    if($contatoDAO->editar($contato)){
        
        $listar=$contatoDAO->listar_todos($usuario["idusuario"]);
        $_SESSION['contatos'] = $listar;
        header("Location: http://localhost/agenda_eletronica/View/admin/admin.php");
    }
    else{
        echo "Erro ao alterar";
    }
   
    }
}

if (isset($_GET['tag'])) {
	$tag=$_GET['tag'];
	if ($tag =="excluir") {
        
		if ($contatoDAO->excluir($_GET['id'])) {
				
            $listar=$contatoDAO->listar_todos($usuario["idusuario"]);
            $_SESSION['contatos'] = $listar;
            header("Location: http://localhost/agenda_eletronica/View/admin/admin.php");

		}
	}else if($tag == "editar") {

		$id = $_GET['id'];
        $contato=$contatoDAO->listar_um($id);
        
        $_SESSION['contato'] = $contato;
        header("Location: http://localhost/agenda_eletronica/View/admin/editar.php");

	}
}

if (isset($_GET['link'])) {
	$link=$_GET['link'];
	if ($link =="deslogar") {
        
        session_destroy();	
        header("Location: http://localhost/agenda_eletronica/View/home.php");
    }elseif($link =="grafico"){
        
        $id = $_GET['idusuario'];
        
        $tipo = 'vivo';
        $vivo=$contatoDAO->grafico($tipo,$id);
        $_SESSION['vivo'] = $vivo;

        $tipo = 'claro';
        $claro=$contatoDAO->grafico($tipo,$id);
        $_SESSION['claro'] = $claro;

        $tipo = 'oi';
        $oi=$contatoDAO->grafico($tipo,$id);
        $_SESSION['oi'] = $oi;

        $tipo = 'tim';
        $tim=$contatoDAO->grafico($tipo,$id);
        $_SESSION['tim'] = $tim;

        $tipo = 'outros';
        $outros=$contatoDAO->grafico($tipo,$id);
        $_SESSION['outros'] = $outros;
        
        header("Location: http://localhost/agenda_eletronica/View/admin/grafico.php");

    }elseif ($link =="home") {
       
        $contato=$contatoDAO->listar_um($usuario["idusuario"]);
        
        $_SESSION['contato'] = $contato;
        header("Location: http://localhost/agenda_eletronica/View/admin/admin.php");
    }
}




?>