<?php
include_once "../Model/Usuario.class.php";
include_once "../Model/UsuarioDAO.class.php";
include_once "../Model/ContatoDAO.class.php";
session_start();

$contatoDAO = new ContatoDAO();

$usuarioDAO = new UsuarioDAO();

if(isset($_POST['acao'])){
    if($_POST['acao'] == "Cadastrar"){
    
     $nome =    trim(strip_tags($_POST['nome']));
     $email =   trim(strip_tags($_POST['email']));
     $senha =   trim(strip_tags((hash('sha512',$_POST['senha']))));
 
    $usuario = new Usuario();
    $usuario->setNome($nome);
    $usuario->setEmail($email);
    $usuario->setSenha($senha);
    $idusuario = $usuarioDAO->inserir($usuario);
    
        if($idusuario > 0){   
            
            $usuario = $usuarioDAO->listar_um($idusuario);
            $_SESSION['usuario'] = $usuario;
            $listar=$contatoDAO->listar_todos($idusuario);
            $_SESSION['contatos'] = $listar;
            header("Location: http://localhost/agenda_eletronica/View/admin/admin.php");
            //include_once "../View/admin/admin.php";
        }
        else{
            echo "<script>alert('Problema para login, tente mais tarde ou entre em contato com o admin do sistema!');</script>";
            include_once "../View/home.php";
        }

    }
    if($_POST['acao'] == "login"){
    
        $email =   trim(strip_tags($_POST['email']));
        $senha =   trim(strip_tags((hash('sha512',$_POST['senha']))));
  
       $usuario = new Usuario();
       $usuario->setEmail($email);
       $usuario->setSenha($senha);
   
       $usuario = $usuarioDAO->listar($usuario);
       
           if($usuario){
            
            $_SESSION['usuario'] = $usuario;
            $listar=$contatoDAO->listar_todos($usuario["idusuario"]);
            $_SESSION['contatos'] = $listar;
            header("Location: http://localhost/agenda_eletronica/View/admin/admin.php");  
            //echo '<script>window.location="http://localhost/agenda_eletronica/View/admin/admin.php";</script>';
           }
           else{
            echo "<script>alert('email ou senha inválidos!');</script>";
           }
   
       }
}


?>


