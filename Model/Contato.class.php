<?php
class Contato {
    private $idcontato;
    private $usuario_idusuario;
    private $nome;
    private $tipo;
    private $telefone;

    public function setIdcontato($idcontato) {
        $this->idcontato=$idcontato;
   }
   
   public function getIdcontato() {
       return $this->idcontato;
   }

	public function setUsuario_idusuario($usuario_idusuario) {
         $this->usuario_idusuario=$usuario_idusuario;
    }
	
    public function getUsuario_idusuario() {
        return $this->usuario_idusuario;
    }

 	public function setNome($nome) {
         $this->nome=$nome;
    }
	
    public function getNome() {
        return $this->nome;
    }
	
	public function setTipo($tipo) {
         $this->tipo=$tipo;
    }
	
    public function getTipo() {
        return $this->tipo;
    }
    public function setTelefone($telefone) {
        $this->telefone=$telefone;
    }
    
    public function getTelefone() {
        return $this->telefone;
    }
   
}