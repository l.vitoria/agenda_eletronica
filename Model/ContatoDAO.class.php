<?php

include_once 'BD.class.php';

class ContatoDAO extends BD{
private $bd; //conexão com o banco
private $tabela; //nome da tabela

    public function __construct() {
        $this->bd = new BD();
        $this->tabela = "contato";
    }

    public function __destruct() {
        unset($this->bd);
    }

    //insere um novo registro na tabela usuário      
    public function inserir($contato) {

        $usuario  = $contato->getUsuario_idusuario();
		$nome     = $contato->getNome();
        $tipo     = $contato->getTipo();
        $telefone = $contato->getTelefone();
        
              
        $sql = ("INSERT INTO $this->tabela (usuario_idusuario,nome,tipo,telefone) values ( :usuario,:nome,:tipo,:telefone)");  

            $retorno = $this->bd->pdo->prepare($sql);
            $retorno->bindParam(':usuario', $usuario);
            $retorno->bindParam(':nome', $nome);
            $retorno->bindParam(':tipo', $tipo);
            $retorno->bindParam(':telefone', $telefone);

            return $retorno->execute();

    }          
    
    public function listar_todos($id){
        $resultado = $this->bd->pdo->query("SELECT * FROM $this->tabela where usuario_idusuario='$id'");
        $dados= $resultado->fetchAll();  
        return $dados;
        
    }

       
    public function listar_um($idcontato){
        try {           
            
           $sql = "SELECT * FROM $this->tabela WHERE idcontato = :idcontato";


           $res  =$this->bd->pdo->prepare($sql);
           
           $res->bindValue(':idcontato', $idcontato);
           $res->execute();
           
          $linha= $res->fetch();

          return $linha;
           
        } catch ( PDOException  $e) {

           print "Erro: Código:" . $e->getCode() . "Mensagem" . $e->getMessage(); }


    }


    public function editar($contato){

        //$idusuario  = $usuario->getUsuario_idusuario();
        $idcontato  = $contato->getIdcontato();
        $nome       = $contato->getNome();
        $tipo       = $contato->getTipo();
        $telefone   = $contato->getTelefone();
               
        
        $msq = "UPDATE $this->tabela SET nome=:nome, tipo=:tipo, telefone=:telefone  where idcontato = :idcontato";

        $retorno = $this->bd->pdo->prepare($msq);
        $retorno->bindParam(':idcontato', $idcontato);
        $retorno->bindParam(':nome', $nome);
        $retorno->bindParam(':tipo', $tipo);
        $retorno->bindParam(':telefone', $telefone);

            //$editar= $this->bd->pdo->exec($msq);
            //$editar = pg_query($msq);
            
            //return $editar;
        return $retorno->execute();
    }

    public function excluir($id) {
        
        $sql = "delete from $this->tabela where idcontato='$id'";
        $retorno= $this->bd->pdo->exec($sql);
            //$retorno = pg_query($sql);
        return $retorno;
    
    }

    public function grafico($tipo,$id){
        
        $resultado = $this->bd->pdo->query("SELECT count(tipo) as numero FROM $this->tabela where tipo='$tipo' and  usuario_idusuario='$id'");
        $dados= $resultado->fetch();  
        return $dados;
        
    }

    

    
}