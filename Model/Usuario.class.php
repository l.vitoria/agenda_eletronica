<?php
class Usuario {
    private $idusuario;
    private $nome;
    private $email;
    private $senha;

	public function setIdusuario($idusuario) {
         $this->idusuario=$idusuario;
    }
	
    public function getIdusuario() {
        return $this->idusuario;
    }

 	public function setNome($nome) {
         $this->nome=$nome;
    }
	
    public function getNome() {
        return $this->nome;
    }
	
	public function setEmail($email) {
         $this->email=$email;
    }
	
    public function getEmail() {
        return $this->email;
    }
    public function setSenha($senha) {
        $this->senha=$senha;
    }
    
    public function getSenha() {
        return $this->senha;
    }
   
}