<?php

include_once 'BD.class.php';

class UsuarioDAO extends BD{
private $bd; //conexão com o banco
private $tabela; //nome da tabela

    public function __construct() {
        $this->bd = new BD();
        $this->tabela = "usuario";
    }

    public function __destruct() {
        unset($this->bd);
    }

    //insere um novo registro na tabela usuário      
    public function inserir($usuario) {

		$nome=$usuario->getNome();
        $email=$usuario->getEmail();
        $senha=$usuario->getsenha();

        
        $sql = ("INSERT INTO $this->tabela (nome,email,senha) values ( :nome,:email,:senha)");  

            $retorno = $this->bd->pdo->prepare($sql);
            $retorno->bindParam(':nome', $nome);
            $retorno->bindParam(':email', $email);
            $retorno->bindParam(':senha', $senha);

            $retorno->execute();
            $id = $this->bd->pdo->lastInsertId(); 
            return $id;

    }          
   
       
    public function listar($usuario){
        try {

            $nome=$usuario->getEmail();
            $sexo=$usuario->getSenha();
           
        $sql = "SELECT * FROM $this->tabela WHERE email = :email and senha = :senha";


           $res  =$this->bd->pdo->prepare($sql);
           
           $res->bindValue(':email', $usuario->getEmail());
           $res->bindValue(':senha', $usuario->getSenha());
           $res->execute();
           
          $linha= $res->fetch();

          return $linha;
           
           
        } catch ( PDOException  $e) {

           print "Erro: Código:" . $e->getCode() . "Mensagem" . $e->getMessage(); }


    }


    public function listar_um($idusuario){
        try {           
            
           $sql = "SELECT * FROM $this->tabela WHERE idusuario = :idusuario";


           $res  =$this->bd->pdo->prepare($sql);
           
           $res->bindValue(':idusuario', $idusuario);
           $res->execute();
           
          $linha= $res->fetch();

          return $linha;
           
        } catch ( PDOException  $e) {

           print "Erro: Código:" . $e->getCode() . "Mensagem" . $e->getMessage(); }


    }

    
}