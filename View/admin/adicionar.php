<?php
session_start();
// Verifica se existe os dados da sessão de login 
if(!isset($_SESSION["usuario"])){ 
    include_once "../Controller/UsuarioController.php";
    // Usuário não logado! Redireciona para a página de login 
    header("Location: http://localhost/agenda_eletronica/View/home.php"); 
    exit; 
} 

$logado = $_SESSION['usuario'];

?>
<!doctype html>
<html lang="pt-br">
  <head>
    <title>Title</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  </head>
  <body>
    <div class="container">
    <nav class="nav justify-content-center">
        <a role="button" class="btn btn-primary m-3" href="http://localhost/agenda_eletronica/Controller/ContatoController.php?link=home">Home</a>
        <a role="button" class="btn btn-secondary m-3"  href="http://localhost/agenda_eletronica/Controller/ContatoController.php?link=deslogar">Sair</a>
    </nav>  
        <form action="http://localhost/agenda_eletronica/Controller/ContatoController.php"  method="post">
            <div class="form-group">
              <label for="nome">Nome</label>
              <input type="text" id="nome" name="nome" class="form-control" placeholder="Seu nome" required>
            </div>
			<div class="form-group">
      <label for="Operadora">Operadora</label>
              <select id="Operadora" class="form-control" name="tipo" id="">
                <option value="oi">oi</option>
                <option value="tim">tim</option>
                <option value="vivo">vivo</option>
                <option value="claro">claro</option>
                <option value="outros">outros</option>
              </select>
            </div>
            <div class="form-group">
              <label for="telefone">Telefone</label>
              <input type="text" id="telefone" oninput="maskInput(event)"  mask="(99)99999-9999" name="telefone" class="form-control" placeholder="telefone" required>
            </div>
            <input type="hidden" name="idusuario" value="<?php echo $logado['idusuario']?>">
            <button type="submit" class="btn btn-primary position-button-cadastrar" name="acao" value="Cadastrar">
			Cadastrar
			</button>
        </form>
    </div>
      
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script src="http://localhost/agenda_eletronica/View/vendor/js/mascara.js"></script>
  </body>
</html>