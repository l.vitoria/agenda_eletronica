<?php
session_start();
// Verifica se existe os dados da sessão de login 
if (!isset($_SESSION["usuario"])) {
  include_once "../Controller/ContatoController.php";
  // Usuário não logado! Redireciona para a página de login 
  echo "deu ruim";
  die;
  header("Location: http://localhost/agenda_eletronica/View/home.php");
  exit;
}

$logado = $_SESSION['usuario'];
$contatos = $_SESSION['contatos'];

?>
<!doctype html>
<html lang="pt-br">

<head>
  <title>Title</title>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>

<body>
  <div class="container-fluid">
    <nav class="nav justify-content-center">
      <a role="button" class="btn btn-primary m-3" href="http://localhost/agenda_eletronica/Controller/ContatoController.php?link=grafico&amp;idusuario=<?php echo $logado['idusuario'] ?>">Gerar gráfico</a>
      <a role="button" class="btn btn-secondary m-3" href="http://localhost/agenda_eletronica/Controller/ContatoController.php?link=deslogar">Sair</a>
    </nav>
    <a role="button" class="btn btn-success mb-3" href="http://localhost/agenda_eletronica/View/admin/adicionar.php">Adicionar</a>
    <div class="table-responsive">
      <table class="table table-striped table-hover">
        <thead class="bg-info">
          <tr>
            <th class="text-white">Nome</th>
            <th class="text-white">Operadora</th>
            <th class="text-white">Telefone</th>
            <th class="text-white">Editar</th>
            <th class="text-white">Excluir</th>
          </tr>
        </thead>
        <tbody>
          <?php foreach ($contatos as $contato) { ?>
            <tr>
              <td scope="row"><?php echo $contato['nome'] ?></td>
              <td><?php echo $contato['tipo'] ?></td>
              <td><?php echo $contato['telefone'] ?></td>
              <td><a role="button" class="btn btn-primary" href="http://localhost/agenda_eletronica/Controller/ContatoController.php?tag=editar&amp;id=<?php echo $contato['idcontato'] ?>">Editar</a></td>
              <td><a role="button" class="btn btn-danger" href="http://localhost/agenda_eletronica/Controller/ContatoController.php?tag=excluir&amp;id=<?php echo $contato['idcontato'] ?>">Excluir</a></td>

            </tr>
          <?php } ?>

        </tbody>
      </table>
    </div>
  </div>

  <!-- Optional JavaScript -->
  <!-- jQuery first, then Popper.js, then Bootstrap JS -->
  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>

</html>