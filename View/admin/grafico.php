<?php
session_start();
// Verifica se existe os dados da sessão de login 
if (!isset($_SESSION["usuario"])) {
    include_once "../Controller/ContatoController.php";
    // Usuário não logado! Redireciona para a página de login 
    header("Location: http://localhost/agenda_eletronica/View/home.php");
    exit;
}

$logado = $_SESSION['usuario'];
$vivo   = $_SESSION['vivo'];
$claro  = $_SESSION['claro'];
$oi     = $_SESSION['oi'];
$tim    = $_SESSION['tim'];
$outros = $_SESSION['outros'];
// print_r($oi['numero']);
// die;


?>
<!doctype html>
<html lang="en">

<head>
    <title>grafico</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>
    <link href="../vendor/css/style_admin.css" rel="stylesheet">
</head>

<body>
    <nav class="nav justify-content-center">
        <a role="button" class="btn btn-primary m-3" href="http://localhost/agenda_eletronica/Controller/ContatoController.php?link=home">Home</a>
        <a role="button" class="btn btn-secondary m-3"  href="http://localhost/agenda_eletronica/Controller/ContatoController.php?link=deslogar">Sair</a>
    </nav>
    <div class="mx-auto my-3" id="chart">
        <canvas id="myChart"></canvas>
    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script>
        var data = {
            datasets: [{
                data: [
                    <?php
                    echo $vivo['numero'] . ",";
                    echo $claro['numero'] . ",";
                    echo $oi['numero'] . ",";
                    echo $tim['numero'] . ",";
                    echo $outros['numero'] . ",";

                    ?>
                ],
                backgroundColor: [
                    "#DF01D7",
                    "#FF0040",
                    "#FFCE56",
                    "#0404B4",
                    "#E7E9ED"
                ],
                label: 'My dataset' // for legend
            }],
            labels: [
                "Vivo",
                "Claro",
                "Oi",
                "Tim",
                "Outros"

            ]
        };
        var ctx = $("#myChart");
        new Chart(ctx, {
            data: data,
            type: 'polarArea'
        });
    </script>
</body>

</html>