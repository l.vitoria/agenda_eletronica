<?php
include_once "../Controller/UsuarioController.php";
?>
<!DOCTYPE html>
<html lang="pt-br">

<head>
  <title>Agenda eletrõnica</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="icon" type="image/png" href="vendor/imagem/logo.png">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,700" rel="stylesheet">
  <!-- slick css-->
  <link href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.7.1/slick.css" rel="stylesheet">
  <link href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.7.1/slick-theme.css" rel="stylesheet">
  <link href="vendor/css/style.css" rel="stylesheet">
</head>

<body>
  <main>
    <!-- parte principal-->
    <div class="loader" id='loader'></div>
    <div class="container-fluid number-presentation-bg menu">
      <div class="container pr-0">
        <button type="button" class="btn btn-primary position-button-login" data-toggle="modal" data-target="#modalExemplo">
          Entrar
        </button>
      </div>
    </div>
    <div class="container-fluid presentation-bg">
      <div class="container">
        <div class="row">
          <div class="col-xl-12">
            <header>

            </header>
            <div class="presentation-box">
              <h1 class="text-white text-right"> <span>Seja bem-vindo a sua agenda eletrônica.</span> </h1>
              <h1 class="text-white text-right"> <span>Aqui você guarda todos os seus números.</span> </h1>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- dicas e noticias-->
    <div class="container">
      <div class="gallery">
        <div class="row">
          <div class="title-notices">
            <h2>Dicas e Notícias</h2>
            <a href="#">Veja todas as Dicas e Notícias</a>
          </div>
          <div class="carousel">
            <div class="col-12">
              <div class="col col-12 col-lg-7">
                <p class="notice-date"> quinta-feira 23/05/2019 </p>
                <img src="vendor/imagem/celular.jpg" class="img-responsive primeira-imagem" alt="noticia">
              </div>
              <div class="col col-12 col-lg-5">
                <h2><b>Lorem ipsum dolor sit amet consectetur adipisicing elit.
                    ed diam, nonummy nibh. </b></h2>
                <p>
                  Lorem, ipsum dolor sit amet consectetur adipisicing elit.
                  Quam sed ad expedita minus sequi, sapiente ipsam ullam pariatur
                  similique dolore consequuntur error officiis adipisci doloremque
                  facere blanditiis commodi libero labore. <a href="#">continue lendo</a>
                </p>
              </div>
            </div>
            <div class="col-12">
              <div class="col col-12 col-lg-7">
                <p class="notice-date"> segunda-feira 26/05/2019 </p>
                <img src="vendor/imagem/cel.jpg" class="img-responsive primeira-imagem" alt="noticia">
              </div>
              <div class="col col-12 col-lg-5">
                <h2><b>Lorem ipsum dolor sit amet consectetur adipisicing elit.
                    ed diam, nonummy nibh. </b></h2>
                <p>
                  Lorem, ipsum dolor sit amet consectetur adipisicing elit.
                  Quam sed ad expedita minus sequi, sapiente ipsam ullam pariatur
                  similique dolore consequuntur error officiis adipisci doloremque
                  facere blanditiis commodi libero labore. <a href="#">continue lendo</a>
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-12 col-md-6">
        <p class="notice-date"> terça-feira 18/01/2019 </p>
        <div class="col-12 col-md-4 px-0">
          <img src="vendor/imagem/not.jpg" class="img-responsive" alt="noticia">
        </div>
        <div class="col-12 col-md-8">
          <h3><b>Lorem ipsum dolor sit amet consectetur ptates aliquid.</b></h3>
        </div>
        <div class="col-sm-12 px-0">
          <p class="notice-desc">Lorem, ipsum dolor sit amet consectetur adipisicing elit.
            Quam sed ad expedita minus sequi, sapiente ipsam ullam pariatur
            similique dolore consequuntur. consequuntur error officiis adipisci dolorem que <a href="#">continue lendo</a> </p>
        </div>
      </div>

      <div class="col-12 col-md-6">
        <p class="notice-date"> quinta-feira 14/01/2019 </p>
        <div class="col-12 col-md-4 px-0">
          <img src="vendor/imagem/not.jpg" class="img-responsive" alt="noticia">
        </div>
        <div class="col-12 col-md-8">
          <h3><b>Lorem ipsum dolor sit amet consectetur adipisicing elit.</b></h3>
        </div>

        <div class="col-sm-12 px-0">
          <p class="notice-desc">Lorem, ipsum dolor sit amet consectetur adipisicing elit.
            Quam sed ad expedita minus sequi, sapiente ipsam ullam pariatur
            similique dolore consequuntur error officiis adipisci doloremque
            facere blanditiis commodi libero labore. <a href="#">continue lendo</a> </p>
        </div>

      </div>
    </div>
    </div>

    <!-- formulario-->
    <div class="container-fluid services">
      <div class="container">
        <div class="row">
          <div class="col col-12 form-response">
            <h2 class=" text-white">Formulário de cadastro </h2>
            <form action="http://localhost/agenda_eletronica/Controller/UsuarioController.php" method="post">
              <div class="form-group">
                <label for="nome">Nome</label>
                <input type="text" id="nome" name="nome" class="form-control" placeholder="Nome" required>
              </div>
              <div class="form-group">
                <label for="email">Email</label>
                <input type="email" id="email" name="email" class="form-control" placeholder="Email" required>
              </div>
              <div class="form-group">
                <label for="senha">Senha</label>
                <input type="password" id="senha" name="senha" class="form-control" placeholder="Senha" required>
              </div>
              <button type="submit" class="btn btn-primary position-button-cadastrar" name="acao" value="Cadastrar">
                Cadastrar
              </button>
            </form>
          </div>
        </div>
      </div>
    </div>
    </div>
    </div>
  </main>

  <!-- rodapé-->
  <footer class="container-fluid text-center my-3">
    <div class="container">
      <div class="footer-content">
        <div class="contact-us">
          <img src="vendor/imagem/logo.png" alt="logo">
          <div class="contact-box">
            <p> <b> <span>(xx)</span> xxxx.xxxx | x xxxxx.xxxx </b> </p>
            <p> xx. xxxxxx xxxx xxxx, xxxx xxxx <br>
              xxxxxxx - xxxx xxxxx - xxxxxx / xx
            </p>
          </div>
        </div>
        <p><strong> xxxxxx </strong></p>
      </div>
    </div>
  </footer>

  <!-- Modal -->
  <div class="modal fade" id="modalExemplo" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Logar</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form action="http://localhost/agenda_eletronica/Controller/UsuarioController.php" method="post">
            <div class="form-group">
              <label for="exampleInputEmail1">Endereço de email</label>
              <input type="email" name="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="email" required>
            </div>
            <div class="form-group">
              <label for="exampleInputPassword1">Senha</label>
              <input type="password" name="senha" class="form-control" id="exampleInputPassword1" placeholder="Senha" minlength="8" required>
            </div>
            <button type="submit" class="btn btn-primary" name="acao" value="login">logar</button>
          </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
        </div>
      </div>
    </div>
  </div>

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>

  <!-- slick javascritp-->
  <script src="https://code.jquery.com/jquery-1.11.0.min.js"></script>
  <script src="https://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.7.1/slick.js"></script>

  <script>
    $('.carousel').slick({
      dots: true,
      infinite: true
    });
  </script>

  <script>
    //o preload denomina o tempo de execução
    window.onload = function() {
      var div = document.getElementById('loader');
      preload(div, 100);
    };

    function preload(el, interval) {
      var op = 1;
      var timer = setInterval(function() {
        if (op <= 0.1) {
          clearInterval(timer);
          el.style.display = 'none';
          el.className = '';
        }
        el.style.opacity = op;
        op -= op * 0.1;
      }, interval);
    }
  </script>


</body>

</html>