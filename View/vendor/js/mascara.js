console.clear()

maskInput = function (event) {
console.clear()

// Catch data
var input  = event.target
var mask   = input.getAttribute('mask')
var value  = input.value.replace(/[^0-9]/g, '')

// Limits
var minLen = mask.replace(/([*][9]|[^0-9])/g, '').split('').length
var maxLen = mask.replace(/[^0-9]/g, '').split('').length
var length = value.split('').length

var regex = ""
var vars = ""
var count = 0
var optional = length - minLen
mask.replace(/([*][9])/g, '*').replace(/[9]/g, '$').split('')
.map(char => {
    switch (char) {
    case "$" :
        count++
        regex += "([\\d]{0,1})"
        vars += '$' + count
    ;break;
    case "*" :
        if (length > minLen && optional > 0) {
        count++
        optional--
        regex += "([\\d]{0,1})"
        vars += '$' + count
        }
    ;break;  
    default:
        vars += char
    ;break;
    }
})
vars = vars.replace(/[\s]{2,}/g, ' ')

var masked = value.slice(0,maxLen)
                    .replace(new RegExp('^' + regex + '$', 'g'), vars)
                    .replace(/[\s]{2,}/g, ' ') // Remove doubles space
                    .replace(/[^0-9]{0,}$/g, '')

input.value = masked
}

var telMask = ['(99) 9999-99999', '(99) 99999-9999'];
var tel = document.querySelector('#telefone');
VMasker(tel).maskPattern(telMask[0]);
tel.addEventListener('input', inputHandler.bind(undefined, telMask, 14), false);
